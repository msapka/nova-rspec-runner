#!/bin/sh

# Subject
parsed_subject=""
if [ "$SUBJECT" == "all files" ];then
  parsed_subject=""
elif [ "$SUBJECT" == "only failed examples" ]; then
  parsed_subject="--only-failures"
elif [ "$SUBJECT" == "next failed example" ]; then
  parsed_subject="--next-fail"
fi


echo "Running: bundle exec rspec --format $FORMAT $parsed_subject $CUSTOM_ARGS"

bundle exec rspec --format $FORMAT $parsed_subject $CUSTOM_ARGS
