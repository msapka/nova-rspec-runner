# Runner template

Provides an runner template for Rspec.

![Screensot](https://gitlab.com/msapka/nova-rspec-runner/-/raw/fef1c9da97050c93f1938efef73eec1dcaf2259f/Rspec%20Runner.novaextension/Images/screenshot.png)

Configurable parameters:
- Format:
  - progress
  - documentation (default)
  - html
  - json
- Subject:
  - all files (default)
  - Only failed examples
  - Next failed example


